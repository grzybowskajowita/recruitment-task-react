import React, { Component } from 'react';
import axios from 'axios';
import List from './List';



class App extends Component {
  constructor(props){
	    super(props);
	  
	  this.state = {  people: [],  }	
	  this.getPeople = this.getPeople.bind(this);
					} //constructor props	
  
  getPeople(){
	  return axios.get("http://swapi.co/api/people/?page=1")
	  .then((response) => {
		  console.log(response.data.results);
		  this.setState( { people: response.data.results } )
	  })
  }
  
  componentDidMount(){  this.getPeople()  }
  
  render() {
	const {people} = this.state;
    return (
      <div className="App">
        <List people={people} />
      </div>
    ); 
  } //render
} //class App extends Component
export default App;
